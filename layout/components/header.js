import React,{ useEffect, useState } from "react";
import Link from 'next/link'
import { Col, Row, Typography } from 'antd'

function Header(props){
    const [ message, setMessage ] = useState('Blog Site')

    let host = typeof window !== "undefined" && window.location && window.location.host ? window.location.host : null

    let res = {}
    useEffect(() => {
        if (host){
            const cmsDetailsURL = `api/v1/management/bloggerdetails/${host}`;
            res = fetch(process.env.NEXT_PUBLIC_API_URL + cmsDetailsURL,{
                    method:'GET',
                    headers: {
                        'Content-Type': 'application/json',
                    },
                }).then(response => response.json())
                .then(data => {
                if (data.status === 0){
                    setMessage(data.data.name)
                }
                })
                .catch((error) => {
                console.error('Error:', error);
                });
        }
    },[res])
    
    return(
        <Row>
            <Col lg={5} xs={5} md={5}>
                <Link href='/' passHref>
                    <Typography.Text>{message}</Typography.Text>
                </Link>
            </Col>
            <Col offset={14} lg={5} xs={5} md={5}>
                <Link href='/contact-us' passHref>
                    <Typography.Text>Contact Us</Typography.Text>
                </Link>
            </Col>
        </Row>
    )
}

export default Header