import { useState , useEffect } from 'react'
import Image from 'next/image'
import Link from 'next/link'
import styles from '../styles/Home.module.css'
import { Card , Typography,Row,Col } from 'antd'

export default function Home() {
  const [ contents , setContents ] = useState([]);
  
  let host = typeof window !== "undefined" && window.location && window.location.host ? window.location.host : null
  
  let res = {}
  useEffect(() => {
    if(host){
      const cmsDetailsURL = `api/v1/consumer/bloglist/${host}`;
          res = fetch(process.env.NEXT_PUBLIC_API_URL + cmsDetailsURL,{
                  method:'GET',
                  headers: {
                      'Content-Type': 'application/json',
                  },
              }).then(response => response.json())
              .then(data => {
              if (data.status === 0 && contents.length === 0){
                  setContents(data.data)
              }
              })
              .catch((error) => {
              console.error('Error:', error);
              });
    }
  },[res])

  return (
    <Row style={{justifyContent: "center"}}>
      <Col xs={20} sm={16} md={12} lg={8} xl={4} >
    {contents && contents.map(content=>{
      
      return <Card
          hoverable
          style={{ width: 240 }}
          cover={<img alt={content.title} src={content.file.length?`${process.env.NEXT_PUBLIC_IMAGE_URL}${content.file[0].url}`:""} />}
        >
          <Link href={`${content.slug}/${content.id}`}>
            <Card.Meta title={content.title} description={content.description} />
          </Link>
        </Card>
      })}
      </Col>
    </Row>
  )
}
