import React, { useState , useEffect } from 'react'
import styles from '../styles/Home.module.css'
import { Input ,Form, Typography,Col , Button } from 'antd'

const formItemLayout = {
    labelCol: {
      span: 4,
    },
    wrapperCol: {
      span: 8,
    },
  };

export default function ContactUs(props) {
    const [form] = Form.useForm();
    const { Title, Text } = Typography;
    const [ success_message, setMessage ] = useState('')

    const submitForm =(values)=>{
        
        values.blogger = props.bloggerData.unique_key
        let contactUsURL = 'management/notification/create'
        const res = fetch(process.env.NEXT_PUBLIC_API_URL + contactUsURL,{
            method:'POST',
            headers: {
                'Content-Type': 'application/json',
              },
            body: JSON.stringify(values),
        }).then(response => response.json())
        .then(data => {
          if (data.status === 0){
              setMessage(data.success[0])
          }
        })
        .catch((error) => {
          console.error('Error:', error);
        });
    }
  return (
    <div>
        <Col offset={8} span={16}>
            <Title // Form's Title
                level={3}
                style={{
                marginBottom: 0,
                paddingTop: 20,
                paddingLeft: 30,
                paddingRight: 30,
                }}
            >
                ✉️ Contact Us!
            </Title>
            <Text // Form's Description
                type="secondary"
                style={{
                paddingLeft: 30,
                paddingRight: 30,
                }}
            >
                Let us know how we can help you.
            </Text>
            <Form name="contact-us"
                form={form}
                onFinish={submitForm}
                layout="vertical">
                <Form.Item {...formItemLayout}
                    name="name"
                    label="Name"
                    rules={[
                    {
                        required: true,
                        message: 'Please input your name',
                    },
                    ]}>
                    <Input placeholder="Please input your name" />
                </Form.Item>
                <Form.Item {...formItemLayout}
                    name="email"
                    label="Email"
                    rules={[
                    {
                        required: true,
                        message: 'Please input your email',
                    },
                    ]}>
                    <Input placeholder="Please input your email" />
                </Form.Item>
                <Form.Item {...formItemLayout}
                    name="subject"
                    label="Subject"
                    rules={[
                    {
                        required: true,
                        message: 'Please input your subject',
                    },
                    ]}>
                    <Input placeholder="Please input your subject" />
                </Form.Item>
                <Form.Item {...formItemLayout}
                    name="message"
                    label="Message"
                    rules={[
                    {
                        required: true,
                        message: 'Please input your Message',
                    },
                    ]}>
                    <Input.TextArea placeholder="Please input your Message" />
                </Form.Item>
                <Form.Item >
                    <Button type="primary" htmlType="submit">Submit</Button>
                </Form.Item>
            </Form>
            <Text>{success_message}</Text>
        </Col>
    </div>
  )
}

export async function getServerSideProps(context) {

    let host = context && context.req && context.req.headers ? context.req.headers.host : ''
    const cmsDetailsURL = `management/bloggerdetails/${host}`;
    if (!process.env.NEXT_PUBLIC_API_URL) {
        throw new Error("Incomplete configuration, please setup API path.");
    }

    const res = await fetch(process.env.NEXT_PUBLIC_API_URL + cmsDetailsURL);
    const data = await res.json();

    if (data && data.data) {
        return {
            props: {
                bloggerData: data.data,
            }

        }
    }
    else {
        return {
            props: {
                bloggerData: null,
            }
        }
    }

}