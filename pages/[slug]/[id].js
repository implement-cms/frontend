import React, { Component } from 'react';
import Banner from '../../components/cms/banner';
import Content from '../../components/cms/content';

export default function CMSContent(props) {

    let cmsContent = props && props.cmsData ? props.cmsData : '';
    let style = props && props.style ? props.style : {};
    const ContentDescription = { whiteSpace: 'pre-line;', paddingLeft: '60px', paddingTop: '50px', textAlign: 'justify' }

    console.log(cmsContent)
    return (
        // <div dangerouslySetInnerHTML={{ __html: content.content }} style={style}>
        // </div>
        <>  
            <Banner data = {cmsContent.file} style={ContentDescription}/>
            <Content data = {cmsContent.content}/>
        </>
    );
}
export async function getServerSideProps(context) {

    let id = context && context.params && context.params.id ? context.params.id : ''
    let slug = context && context.params && context.params.slug ? context.params.slug : ''
    const cmsDetailsURL = `api/v1/consumer/pages/${slug}/${id}`;
    if (!process.env.NEXT_PUBLIC_API_URL) {
        throw new Error("Incomplete configuration, please setup API path.");
    }

    const res = await fetch(process.env.NEXT_PUBLIC_API_URL + cmsDetailsURL);
    const data = await res.json();

    if (data && data.data) {
        return {
            props: {
                cmsData: data.data,
            }

        }
    }
    else {
        return {
            props: {
                cmsData: null,
            }
        }
    }

}