import React, { useState, useEffect } from 'react';
import { Spin, Layout, Row } from 'antd';
import Topbar from '../components/header';
import SiteStyle from '../../styled/cms-styles'
// import BottomBar from '../components/footer';

const { Content, Footer, Header } = Layout;
function Master({ children }) {

    return (
        <SiteStyle>
            <Layout className="site-layout" >
                <Header className="site-header"><Topbar/></Header>
                <Content className="site-content">{children}</Content>
                {/* <Footer className="site-footer"><BottomBar /></Footer> */}
            </Layout>
        </SiteStyle>
    );
};

export default Master
