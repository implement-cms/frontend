import React, { Component } from 'react';


export default function Content(props) {

    let content = props && props.data ? props.data : '';
    let style = props && props.style ? props.style : {};
    return (
        <div dangerouslySetInnerHTML={{ __html: content }} style={style}>
        </div>
    );
}