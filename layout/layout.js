import React, { useEffect } from 'react';
import { connect, useDispatch, useSelector } from 'react-redux';
import Master from './default/master';
import AppLayout from './app/master';

const MasterLayout = (props) => {

    return (
        <React.Fragment>

            <Master>
                {props.children}
            </Master>

                
            

            {/*<AppLayout>
                {props.children}
            </AppLayout>} */}

        </React.Fragment>
    );
}

export default MasterLayout;