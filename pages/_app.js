import 'antd/dist/antd.css';
import Master from '../layout/layout';

import Router from 'next/router';
import NProgress from 'nprogress'; //nprogress module
import 'nprogress/nprogress.css'; //styles of nprogress
//Binding events. 
NProgress.configure({ showSpinner: false });
Router.events.on('routeChangeStart',
  () => NProgress.start()); Router.events.on('routeChangeComplete',
    () => NProgress.done()); Router.events.on('routeChangeError',
      () => NProgress.done());

function MyApp({ Component, pageProps }) {
  return (
    <Master>
      <Component {...pageProps} />
    </Master>
    )
}

export default MyApp
