import React, { useState, useEffect } from 'react';
import { Spin, Layout, Row } from 'antd';
import TopbarApp from '../components/header';
import SiteStyle from '../../styled/cms-styles'


const { Content, Footer, Header } = Layout;
function Master({ children }) {
    
    return (
        <SiteStyle>
            <Layout className="site-layout" >
                <Header className="site-header"><TopbarApp /></Header>
                <Content className="site-content">{children}</Content>
                {/* <Footer className="site-footer-app"><BottomBar /></Footer> */}
            </Layout>
        </SiteStyle>
    );
};

export default Master
