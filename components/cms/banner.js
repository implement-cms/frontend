import React, { Component } from 'react';
import { Carousel, Card, Row, Col } from 'antd';
import {getImageURL} from '../utils/utils';

const { Meta } = Card;
export default function Banner(props) {

    let infiniteSlider = props && props.data ? props.data : []
console.log(props)
    return (
        <div>
            <Carousel autoplay>
                {infiniteSlider && infiniteSlider.map(item => (
                    <img style={{ height: '5%' }}
                        key={`infinite-slider--key${item.id}`}
                        src={getImageURL(item.url)}
                    />
                ))}
            </Carousel>
        </div>
    );
}
