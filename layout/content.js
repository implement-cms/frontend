import {Layout } from 'antd';

const { Content, } = Layout;
function Content({ children }) {
    return (
        <Layout.Content
            className="isomorphicContent mastercontent">
            {children && children._self && children._self.props &&
            children._self.props.router && children._self.props.router.asPath == '/checkout' ?
            '' : children && children._self && children._self.props && children._self.props.router &&
            children._self.props.router.asPath == '/uploadprescription' ? '' :null}
            {children}
        </Layout.Content>
    );
};

export default Content
